/*
 * Copyright (c) 2013 WDCY Information Technology Co. Ltd
 * www.wdcy.cc
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * WDCY Information Technology Co. Ltd ("Confidential Information").
 * You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you
 * entered into with WDCY Information Technology Co. Ltd.
 */
package com.andaily.web.controller.validator.developer;

import com.andaily.domain.dto.team.TeamFormDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author Shengzhao Li
 */
@Component
public class TeamFormDtoValidator implements Validator {


    @Override
    public boolean supports(Class<?> clazz) {
        return TeamFormDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "team.form.validation.name.required", "Team name is required");

        TeamFormDto formDto = (TeamFormDto) target;
        if (formDto.getMasters() == null || formDto.getMasters().isEmpty()) {
            errors.rejectValue("masters", "team.form.validation.scrum.master.required", "Please choose Scrum master");
        }

        if (formDto.getMembers() == null || formDto.getMembers().isEmpty()) {
            errors.rejectValue("members", "team.form.validation.scrum.member.required", "Please choose Scrum members");
        }

    }
}