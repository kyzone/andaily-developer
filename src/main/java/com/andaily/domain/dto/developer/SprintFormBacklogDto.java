package com.andaily.domain.dto.developer;

import com.andaily.domain.developer.Backlog;
import com.andaily.domain.dto.SimpleDisabledDto;
import com.andaily.domain.dto.developer.commons.BacklogDocumentDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Date: 13-9-11
 *
 * @author Shengzhao Li
 */
public class SprintFormBacklogDto extends SimpleDisabledDto {

    private int estimateTime;
    private List<BacklogDocumentDto> documentDtos = new ArrayList<>();


    public SprintFormBacklogDto() {
    }

    public SprintFormBacklogDto(Backlog backlog) {
        super(backlog.guid(), backlog.content(), false);
        this.estimateTime = backlog.estimateTime();
        this.documentDtos = BacklogDocumentDto.toBacklogDtos(backlog.documents());
    }

    public SprintFormBacklogDto(String guid, String name, boolean disabled) {
        super(guid, name, disabled);
    }

    public SprintFormBacklogDto(String guid, String name, boolean selected, boolean disabled) {
        super(guid, name, selected, disabled);
    }

    public List<BacklogDocumentDto> getDocumentDtos() {
        return documentDtos;
    }

    public void setDocumentDtos(List<BacklogDocumentDto> documentDtos) {
        this.documentDtos = documentDtos;
    }

    public int getEstimateTime() {
        return estimateTime;
    }

    public SprintFormBacklogDto setEstimateTime(int estimateTime) {
        this.estimateTime = estimateTime;
        return this;
    }
}
