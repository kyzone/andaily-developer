package com.andaily.service.impl;

import com.andaily.domain.dto.user.AndailyUserDetails;
import com.andaily.domain.user.User;
import com.andaily.domain.user.UserRepository;
import com.andaily.infrastructure.support.LogHelper;
import com.andaily.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author Shengzhao Li
 */
public class SecurityServiceImpl implements SecurityService {

    private static LogHelper log = LogHelper.create(SecurityServiceImpl.class);

    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Load user by username: " + username);
        User user = userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException("Not found any LoginUser by [" + username + "] or it is disabled");
        }
        return new AndailyUserDetails(user);
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
