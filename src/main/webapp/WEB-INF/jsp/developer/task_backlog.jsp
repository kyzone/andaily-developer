<%--
  User: Shengzhao Li
  Date: 13-9-15
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="well">
    <div>${backlogDto.name} (<strong>${backlogDto.estimateTime}</strong>)</div>
    <div>
        <ul class="unstyled">
            <c:forEach items="${backlogDto.documentDtos}" var="d">
                <li>
                    <a href="${contextPath}/developer/file/download/${d.fileDto.guid}">${d.fileDto.name}</a>
                    <span class="muted">(${d.fileDto.sizeAsText})</span>
                </li>
            </c:forEach>
        </ul>
    </div>
</div>